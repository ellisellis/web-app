import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ApiService } from '../../services/api.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-solid-svg-icons';

export interface JournalEntry {
  date: string;
  text: string;
}

export interface SafeJournalEntry {
  date: string;
  text: SafeHtml;
}

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.css']
})
export class JournalComponent implements OnInit {
  public entries: Observable<SafeJournalEntry[]>;
  public errorMessage: string;
  public shownIndex: number;
  public faMinusSquare = faMinusSquare;
  public faPlusSquare = faPlusSquare;

  constructor(private api: ApiService, private sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.entries = this.api.getTeamDiaries().pipe(
      map(entries =>
        entries.map(entry => {
          return {
            date: entry.date,
            text: this.sanitizer.bypassSecurityTrustHtml(entry.text),
            threshold: entry.threshold
          };
        }).sort( (a,b) => a.threshold <= b.threshold ? -1 : 1)
      ),
      catchError(this.handleError.bind(this))
    );
  }

  handleError(err: any) {
    this.errorMessage = err;
    return throwError(err);
  }
}
