import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { Observable, of } from 'rxjs';
import { JwtHelperService } from '../../../../node_modules/@auth0/angular-jwt';
import {
  faEye,
  faEyeSlash
} from '../../../../node_modules/@fortawesome/free-solid-svg-icons';
export interface Profile {
  completed: any[];
  token: string;
}

export interface Creds {
  password: string;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public readonly title = 'Bostonshire';
  public teamName = 'traveler';
  public errorMessage: string;
  public totalScore = 1000;
  public teamScore: Observable<number> = of(0);
  public teamDiariesCount = 0;
  public showPassword = false;
  public faEye = faEye;
  public faEyeSlash = faEyeSlash;

  @ViewChild('loginInput') form: ElementRef;

  public loginForm = this.fb.group({
    password: ['', Validators.required]
  });

  private get _showPassword() {
    return this.showPassword;
  }

  private set _showPassword(value) {
    this.showPassword = Boolean(value);
  }

  constructor(
    public auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private api: ApiService,
    private jwt: JwtHelperService
  ) {}

  public refresh() {
    this.teamScore = this.api.getTeamScore();
    this.teamScore.subscribe(this.updateTeamDiariesCount.bind(this));
  }
  toggleShowPassword() {
    this._showPassword = !this._showPassword;
  }
  onSubmit(credentials: Creds) {
    if ( !credentials || !credentials.password) {
      return;
    }

    const creds: Creds = {
      password: credentials.password.toLowerCase()
    };
    this.auth.login(creds).subscribe((response: any) => {
      this.errorMessage = null;
      this.auth.finishAuthentication(response.token);
      if (this.auth.isAdmin()) {
        this.router.navigate(['admin']);
      } else {
        this.ngOnInit();
      }
    }, err => (this.errorMessage = err));
  }

  onError(err: Error) {
    this.errorMessage = err.message;
  }

  updateTeamDiariesCount( totalPoints: any) {
    this.teamDiariesCount = Math.min(Math.floor(totalPoints / 100), 10);
  }

  ngOnInit() {
    if (this.auth.isAuthenticated()) {
      this.teamName = this.jwt.decodeToken().username;
      this.teamScore = this.api.getTeamScore();
      this.teamScore.subscribe(this.updateTeamDiariesCount.bind(this));
    } else {
      this.teamName = 'traveler';
    }
  }
}
