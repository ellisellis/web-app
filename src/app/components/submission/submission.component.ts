import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { ApiService } from '../../services/api.service';
import { fromEvent } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { Challenge } from '../challenges/challenges.component';
import { throttleTime } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import { faFileImage } from '@fortawesome/free-solid-svg-icons';

const FAILURES = [
  'incorrect answer...',
  'please try again...',
  'try again...',
  'incorrect, try again...',
  'nope...',
  'sorry...',
  'that\'s not right...',
  'that\'s not the answer we\'re looking for...'];

function getRandomFailureMessage() {
  const ind = Math.floor(Math.random() * 8);
  return FAILURES[ind];
}

export interface Submission {
  _id: string;
  challenge_id: string;
  team_id: string;
  team_name: string;
  messages: any[];
  attempts: Attempt[];
  status: string;
}

export interface Attempt {
  createdAt?: Date;
  updatedAt?: Date;
  text?: string;
  imageUrl?: string;
}
@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.css']
})
export class SubmissionComponent implements AfterViewInit {
  public text: string;
  public showCompleted = false;
  public completed: boolean;
  public showMessages = false;
  public showAllAttempts = false;
  public faImage = faFileImage;
  @Input() submission: Submission;
  @Input() challenge: Challenge;
  @Output() OnRequestEnd = new EventEmitter<any>();
  @Output() OnRequestStart = new EventEmitter<any>();

  @ViewChild('approveButton') approveButton: ElementRef;
  @ViewChild('denyButton') denyButton: ElementRef;

  constructor(private api: ApiService, private sanitizer: DomSanitizer) {}

  ngAfterViewInit() {
    if (!this.approveButton || !this.denyButton) {
      return;
    }
    fromEvent(this.approveButton.nativeElement, 'click')
      .pipe(throttleTime(5e3))
      .subscribe(() => {
        this.approveSubmission();
      });

    fromEvent(this.denyButton.nativeElement, 'click')
      .pipe(throttleTime(5e3))
      .subscribe(() => {
        this.sendMessage();
      });
  }

  public sanitize(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `${environment.imageHost}/${url}`
    );
  }

  public approveSubmission() {
    const sub = this.submission;
    this.api.patchSubmission(sub._id, { status: 'completed' }).subscribe(
      response => {
        this.OnRequestEnd.emit(response);
      },
      err => {
        this.OnRequestEnd.emit(err);
      }
    );
  }
  public sendMessage() {
    const message = getRandomFailureMessage();
    this.api
      .patchSubmission(this.submission._id, { message, status: 'responded' })
      .subscribe(
        (response: any) => {
          this.OnRequestEnd.emit(response);
        },
        err => {
          this.OnRequestEnd.emit(err);
        }
      );
  }
}
