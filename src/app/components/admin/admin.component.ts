import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { SwUpdate, SwPush } from '@angular/service-worker';
import { Submission } from '../submission/submission.component';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { Challenge } from '../challenges/challenges.component';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public submissions: Observable<Submission[]>;
  public completed: Submission[] = [];
  public pending: Submission[] = [];
  public responded: Submission[] = [];
  public groupToShow = 0;
  public loading = false;
  public faSincAlt = faSyncAlt;

  constructor(
    private api: ApiService,
    private swu: SwUpdate,
    private swPush: SwPush,
    private sanitizer: DomSanitizer
  ) {}

  public refresh() {
    this.ngOnInit();
  }

  public handleResponse(response: any) {
    if (response instanceof Error) {
      this.loading = false;
      console.error(response.message);
      window.alert('something went wrong...');
      return;
    }
    this.ngOnInit();
  }
  public requestStart(response: any) {
    this.loading = true;
  }

  public sanitize(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `${environment.imageHost}/${url}`
    );
  }

  async requestSub() {
    if (this.swu.activated && this.swu.available && this.swu.isEnabled) {
      const sub = await this.swPush.requestSubscription({
        serverPublicKey:
          'BPKpBdiLP2pZWG_vEQTTqZvlldRwuNnm5ttqV1gekpse9gtWimb0ZQGcZBA-rfJOtvhk69L2ZBk9YNw-RD4pmoI'
      });
      this.api.addSubscription(sub).subscribe(response => {});
    }
  }

  public getChallenge(id): Challenge {
    return this.api.getChallenge(id);
  }

  ngOnInit() {
    this.loading = true;
    this.api.getSubmissions().subscribe(submissions => {
      this.loading = false;
      this.completed = submissions
        .filter(submission => submission.status === 'completed')
        .sort(submissionSort);
      this.responded = submissions
        .filter(submission => submission.status === 'responded')
        .sort(submissionSort);
      this.pending = submissions
        .filter(submission => submission.status === 'pending')
        .sort(submissionSort);
    });

    this.requestSub();
  }
}

function submissionSort(a: Submission, b: Submission): number {
  const aLastAttempt =
    a.attempts && a.attempts.length ? a.attempts.slice(-1).pop() : null;
  const bLastAttempt =
    b.attempts && b.attempts.length ? b.attempts.slice(-1).pop() : null;

  if (!aLastAttempt && bLastAttempt) {
    return 1;
  } else if (aLastAttempt && !bLastAttempt) {
    return -1;
  } else {
    return bLastAttempt.createdAt > aLastAttempt.createdAt ? -1 : 1;
  }
}
