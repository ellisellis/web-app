import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public errorMessage: string;
  public loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  public signupForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  onLoginSubmit(credentials: any) {
    this.auth.login(credentials).subscribe((response: any) => {
      this.auth.finishAuthentication(response.token);
      this.router.navigate(['admin']);
    }, err => (this.errorMessage = err));
  }
  onSignupSubmit(credentials: any) {
    this.auth
      .signup(credentials)
      .subscribe(
        (res: { token: string }) => {
          this.auth.finishAuthentication(res.token);
          this.router.navigate(['admin']);
        },
        err => (this.errorMessage = err)
      );
  }

  ngOnInit() {}
}
