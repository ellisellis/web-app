import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable, combineLatest } from 'rxjs';
import { Router } from '@angular/router';
import { Submission } from '../submission/submission.component';
import { faCamera, faPenSquare } from '@fortawesome/free-solid-svg-icons';

export interface Challenge {
  _id: string;
  physical_map_id: number;
  point_value: number;
  description: string;
  challenge_type: string;
  short_name?: string;
  completed?: boolean;
  specific?: string;
  location_type?: string;
  submission?: Submission;
}

export interface ChallengeUI {
  complete?: Challenge[];
  incomplete?: Challenge[];
}
@Component({
  selector: 'app-challenges',
  templateUrl: './challenges.component.html',
  styleUrls: ['./challenges.component.css']
})
export class ChallengesComponent implements OnInit {
  public challengeModel: Observable<ChallengeUI>;
  public showGroup = 0;
  public faCamera = faCamera;
  public faPenSquare = faPenSquare;
  public loading: boolean;
  constructor(private api: ApiService, private router: Router) {}

  onClick(challenge: Challenge) {
    const id = challenge._id;
    this.router.navigate(['/challenges', id]);
  }

  public refresh() {
    this.ngOnInit();
  }

  ngOnInit() {
    this.loading = true;
    const challenges = this.api.getChallenges();
    this.challengeModel = combineLatest(
      this.api.getCompleted(),
      this.api.getSubmissions(),
      (completed: string[], submissions: Submission[]): ChallengeUI => {
        return challenges
          .map<Challenge>(
            (challenge: Challenge): Challenge => {
              const matchingSubmission = submissions
                .filter(sub => sub.challenge_id === challenge._id)
                .pop();
              return {
                ...challenge,
                completed: completed.includes(challenge._id),
                submission: matchingSubmission
              };
            }
          )
          .reduce<ChallengeUI>(
            (acc, challenge) => {
              if (challenge.completed) {
                acc.complete.push(challenge);
              } else {
                acc.incomplete.push(challenge);
                acc.incomplete = acc.incomplete.sort(challengeSort);
              }
              return acc;
            },
            { complete: [], incomplete: [] }
          );
      }
    );
    this.challengeModel.subscribe( () => {
      this.loading = false;
    })
  }
}

function challengeSort(a: Challenge, b: Challenge): number {
  const aStatus: string = a.submission ? a.submission.status : 'unsubmitted';
  const bStatus: string = b.submission ? b.submission.status : 'unsubmitted';

  switch (aStatus) {
    case 'responded': {
      return -1;
    }
    case 'pending': {
      return bStatus === 'responded' ? 1 : bStatus === 'pending' ? 0 : -1;
    }
    default: {
      const aId = a.physical_map_id;
      const bId = b.physical_map_id;
      return aId <= bId ? -1 : 1;
    }
  }
}
