import {
  Component,
  OnInit,
  ElementRef,
  AfterViewInit,
  ViewChild
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throttleTime } from 'rxjs/operators';
import {
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/animations';
import { Challenge } from '../challenges.component';
import { ApiService } from '../../../services/api.service';
import { fromEvent } from 'rxjs';
import { Submission } from '../../submission/submission.component';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';
import {
  faCamera,
  faPenSquare,
  faImage
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-challenge',
  templateUrl: './challenge.component.html',
  styleUrls: ['./challenge.component.css']
})
export class ChallengeComponent implements OnInit, AfterViewInit {

  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  public title: string;
  public challenge: Challenge;
  public submission: Submission = null;
  public loading: boolean;
  public error: string;

  public faCamera = faCamera;
  public faPenSquare = faPenSquare;
  public faImage = faImage;

  private fileToUpload: File;

  @ViewChild('submitImageBtn') submitImageBtn: ElementRef;
  @ViewChild('submitTextBtn') submitTextBtn: ElementRef;

  @ViewChild('fileLabel') fileLabel: ElementRef;
  @ViewChild('imageSubmission') imageInput: ElementRef;
  @ViewChild('textSubmission') textInput: ElementRef;

  @ViewChild('inputForm') formy: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiService,
    public sanitizer: DomSanitizer
  ) {}

  ngAfterViewInit() {
    this.addListeners();
  }

  public sanitize(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `${environment.imageHost}/${url}`
    );
  }

  private handleSuccess(response: any) {
    this.loading = false;
    this.router.navigate(['challenges']);
  }

  private handleError(response: any) {
    this.loading = false;

    if (response && response.message) {
      if (/413/.test(response.message)) {
        window.alert(
          'Sorry, that file was too large, please keep image size under 7MB'
        );
      } else {
        console.error(response);
        window.alert(
          'Something went wrong...\n If this problem persists, contact Boxaroo team'
        );
      }
    }
    this.ngOnInit();
  }

  onSubmit(type: string, value: HTMLInputElement, challenge: Challenge) {
    this.loading = true;
    this.error = null;
    const submission = type === 'image' ? this.fileToUpload : value.value;

    if (!submission) {
      return;
    }
    this.api
      .submitChallenge(challenge, submission)
      .subscribe(this.handleSuccess.bind(this), this.handleError.bind(this));
  }

  handleFileInput(files: FileList) {
    const file = files.item(0);
    if ( !file ) {
      return;
    }
    if (file.size > 1024 * 1024 * 10) {
      this.error = `File is ${(file.size / (1024 * 1024)).toFixed(
        1
      )}MB, please keep under 10MB`;
    } else {
      this.error = null;
    }

    const fileName = file.name;
    this.fileLabel.nativeElement.innerHTML = fileName;

    this.fileToUpload = files.item(0);
  }

  addListeners() {
    switch (this.challenge.challenge_type) {
      case 'image': {
        fromEvent(this.submitImageBtn.nativeElement, 'click')
          .pipe(throttleTime(5e3))
          .subscribe(() => {
            this.onSubmit(
              'image',
              this.imageInput.nativeElement,
              this.challenge
            );
          });
        break;
      }
      case 'text': {
        fromEvent(this.submitTextBtn.nativeElement, 'click')
          .pipe(throttleTime(5e3))
          .subscribe(() => {
            this.onSubmit('text', this.textInput.nativeElement, this.challenge);
          });
        break;
      }
      default:
        break;
    }
  }

  ngOnInit() {
    this.loading = true;
    this.route.data.subscribe((data: any[]) => {
      const [model] = data;

      this.challenge = model.challenge;
      this.submission = model.submissions;
      this.loading = false;
    });
  }
}
