import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { ROUTES } from './app.routes';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './components/profile/profile.component';
import { JournalComponent } from './components/entries/entries.component';
import { ChallengesComponent } from './components/challenges/challenges.component';
import { DiaryComponent } from './components/diary/diary.component';
import { ChallengeComponent } from './components/challenges/challenge/challenge.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminComponent } from './components/admin/admin.component';
import { SubmissionComponent } from './components/submission/submission.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

export function jwtTokenGetter() {
  return localStorage.getItem('token');
}

const jwtOpts: JwtModuleOptions = {
  config: {
    tokenGetter: jwtTokenGetter,
    whitelistedDomains: [
      'localhost:3001',
      'localhost:3001/',
      'bostonshire.com',
      'bostonshire.com/',
      'api.devlemur.com',
      'api.devlemur',
      'api.devlemur.com/api/*',
      'api.devlemur.com:4001',
      'dev.devlemur.com',
      'dev.devlemur',
      'dev.devlemur.com:4001',
      'dev.devlemur.com/',
      'dev.devlemur/',
      'dev.devlemur.com:4001/'
    ]
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    JournalComponent,
    ChallengesComponent,
    DiaryComponent,
    ChallengeComponent,
    AdminComponent,
    SubmissionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
    RouterModule.forRoot(ROUTES),
    JwtModule.forRoot(jwtOpts),
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production && environment.sw
    })
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
