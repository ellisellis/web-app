import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ChallengesComponent } from './components/challenges/challenges.component';
import { DiaryComponent } from './components/diary/diary.component';
import { AuthGuard, RoleGuard } from './services/auth.service';
import { ChallengeComponent } from './components/challenges/challenge/challenge.component';
import { AdminComponent } from './components/admin/admin.component';
import { SubmissionComponent } from './components/submission/submission.component';
import { ChallengeDataResolver } from './services/challenge-data.service';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard, RoleGuard]
  },
  {
    path: 'admin/submissions/:id',
    component: SubmissionComponent,
    canActivate: [AuthGuard, RoleGuard]
  },
  { path: 'admin-login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  {
    path: 'challenges',
    component: ChallengesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'challenges/:id',
    component: ChallengeComponent,
    canActivate: [AuthGuard],
    resolve: [ChallengeDataResolver]
  },
  { path: 'diary', component: DiaryComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' }
];
