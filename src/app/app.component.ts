import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private endTime = new Date('2018-08-19T20:00:00.000Z');
  public headHome = Date.now() >= this.endTime.getTime();
  constructor( public auth: AuthService ) {
    setInterval(() => {
      this.headHome = Date.now() >= this.endTime.getTime();
    }, 5000);
  }
}
