import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { JournalEntry } from '../components/entries/entries.component';
import { Observable, throwError, combineLatest } from 'rxjs';
import {
  catchError,
  map,
  reduce,
  throttle,
  throttleTime
} from 'rxjs/operators';
import { Challenge } from '../components/challenges/challenges.component';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Submission } from '../components/submission/submission.component';
import { challenges } from './challenges';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private host = `${environment.apiHost}/api`;
  constructor(private http: HttpClient, private jwt: JwtHelperService) {}

  getJournalEntries(): Observable<JournalEntry[]> {
    return this.http
      .get<JournalEntry[]>(`${this.host}/entries`)
      .pipe(catchError(this.handleError.bind(this, '/entries')));
  }

  getChallenges(): Challenge[] {
    return challenges;
  }

  getChallenge(id: string): Challenge {
    return challenges.filter(challenge => challenge._id === id).pop();
  }

  getSubmissions(): Observable<Submission[]> {
    return this.http.get<any[]>(`${this.host}/submissions`);
  }

  getSubmissionsByChallenge(challengeId: string): Observable<Submission> {
    const { sub } = this.jwt.decodeToken();
    return this.http
      .get<Submission>(`${this.host}/submissions/team/${sub}/${challengeId}`)
      .pipe(catchError(this.handleError.bind(this, '/submissions')));
  }

  getImage(submissionId: string, attemptId: string): Observable<Blob> {
    const url = `${this.host}/submissions/${submissionId}/image/${attemptId}`;
    return this.http.get(url, { responseType: 'blob' });
  }

  getSubmission(id: string): Observable<any> {
    const submissionURL = `${this.host}/submissions/${id}`;
    return this.http
      .get(submissionURL)
      .pipe(catchError(this.handleError.bind(this, '/submissions/:id')));
  }

  patchSubmission(id: string, payload: any) {
    const submissionURL = `${this.host}/submissions/${id}`;
    return this.http.patch(submissionURL, payload).pipe(throttleTime(5));
  }

  getCompleted(): Observable<any> {
    const { sub } = this.jwt.decodeToken();
    return this.http.get<any>(`${this.host}/profile/${sub}`).pipe(
      map(response => response.completed),
      catchError(this.handleError.bind(this, '/profile'))
    );
  }

  submitChallenge(
    challenge: Challenge,
    submission: File | string
  ): Observable<any> {
    const challenge_id = challenge._id;
    const type = challenge.challenge_type;

    const endpoint = `${this.host}/submissions/${type}/${challenge_id}`;
    let payload;
    switch (type) {
      case 'image': {
        payload = new FormData();
        payload.append('fileKey', submission, (submission as File).name);
        payload.append('type', 'image');
        break;
      }
      case 'text': {
        payload = { text: submission, type: 'text' };
        break;
      }
      default:
        break;
    }
    return this.http.post(endpoint, payload);
  }

  getTeamScore(): Observable<number> {
    return this.getCompleted().pipe(
      map(completed => {
        return challenges
          .filter(challenge => completed.includes(challenge._id))
          .reduce(
            (aggregate, challenge: Challenge) =>
              aggregate + challenge.point_value,
            0
          );
      })
    );
  }

  getTeamDiaries(): Observable<any> {
    const teamId = this.jwt.decodeToken().sub;
    return this.http.get<any>(`${this.host}/entries/team/${teamId}`).pipe(
      map(res => res.entries),
      catchError(this.handleError.bind(this, '/submissions'))
    );
  }

  addSubscription(sub: PushSubscription): Observable<any> {
    return this.http
      .post(`${this.host}/subscriptions`, sub)
      .pipe(catchError(this.handleError.bind(this, '/submissions')));
  }

  private handleError(route: string, error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError('ApiService request failed on route ' + route);
  }
}
