import { TestBed, inject } from '@angular/core/testing';

import { ChallengeDataResolver } from './challenge-data.service';

describe('ChallengeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChallengeDataResolver]
    });
  });

  it('should be created', inject(
    [ChallengeDataResolver],
    (service: ChallengeDataResolver) => {
      expect(service).toBeTruthy();
    }
  ));
});
