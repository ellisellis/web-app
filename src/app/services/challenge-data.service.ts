import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '../../../node_modules/@angular/router';
import { Challenge } from '../components/challenges/challenges.component';
import { ApiService } from './api.service';
import { Observable, combineLatest } from '../../../node_modules/rxjs';
import { map } from '../../../node_modules/rxjs/operators';
import { Submission } from '../components/submission/submission.component';

export interface ChallengeData {
  challenge: Challenge;
  submissions: Submission[];
}

@Injectable({
  providedIn: 'root'
})
export class ChallengeDataResolver implements Resolve<ChallengeData> {
  constructor(private api: ApiService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ChallengeData> {
    const id = route.paramMap.get('id');
    return combineLatest<ChallengeData>(
      this.api.getCompleted(),
      this.api.getSubmissionsByChallenge(id),
      (completed: string[], submissions: Submission[]) => {
        const challenge = this.api.getChallenge(id);

        return {
          challenge: {
            ...challenge,
            completed: completed.includes(challenge._id)
          },
          submissions
        };
      }
    );
  }
}
