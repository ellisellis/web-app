import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, CanActivate } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Creds } from '../components/home/home.component';

export interface TokenResponse {
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_URL = `${environment.apiHost}/api`;

  constructor(
    private http: HttpClient,
    private jwt: JwtHelperService,
    private router: Router
  ) {}

  login(credentials: Creds): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(
      `${this.API_URL}/users/authenticate`,
      credentials
    );
  }

  passwordOnlyLogin(id: string): Observable<{ token: string }> {
    return this.http.get<{ token: string }>(`${this.API_URL}/profile/${id}`);
  }

  signup(credentials: any): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(
      `${this.API_URL}/users`,
      credentials
    );
  }

  finishAuthentication(token: string): void {
    localStorage.setItem('token', token);
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['']);
  }

  isAuthenticated(): boolean {
    return !this.jwt.isTokenExpired();
  }

  isAdmin(): boolean {
    const decoded = this.jwt.decodeToken();
    return decoded && decoded.role === 'admin';
  }

  getExpiry(): Date {
    return this.jwt.getTokenExpirationDate();
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      return false;
    }
    return true;
  }
}

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (this.auth.isAuthenticated() && this.auth.isAdmin()) {
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }
}
