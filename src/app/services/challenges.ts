import { Challenge } from '../components/challenges/challenges.component';

export const challenges: Challenge[] = [
  {
    _id: '5b788a7ed10c2a62562e1bb8',
    physical_map_id: 1,
    point_value: 120,
    short_name: 'The Taza Chocolate Bar',
    specific: '',
    location_type: '',
    description:
      'Head to The Taza Chocolate Bar at The Boston Public Market and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bb9',
    physical_map_id: 3,
    point_value: 120,
    short_name: 'Urban AdvenTours',
    specific: '',
    location_type: '',
    description:
      'Head to Urban AdvenTours and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bba',
    physical_map_id: 5,
    point_value: 120,
    short_name: 'Boxaroo',
    specific: '',
    location_type: '',
    description:
      'Head to Boxaroo and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bbb',
    physical_map_id: 6,
    point_value: 120,
    short_name: 'WalkBoston',
    specific: 'WalkBoston will have a table in the courtyard of Old City Hall',
    location_type: '',
    description:
      'Head to WalkBoston and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bbc',
    physical_map_id: 4,
    point_value: 120,
    short_name: 'Pepper Palace',
    specific:
      'Pepper Palace is in one of the side buildings at Quincy Market, in the basement',
    location_type: '',
    description:
      'Head to Pepper Palace and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bbd',
    physical_map_id: 0,
    point_value: 75,
    short_name: 'Anywhere!',
    specific: '',
    location_type: '',
    description:
      "Take a team photo anywhere you'd like, raising your flag as high as you can!",
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bbe',
    physical_map_id: 0,
    point_value: 20,
    short_name: 'Secret Location!',
    specific: '',
    location_type: '',
    description: "Secret challenge. We believe in you... you've got this!",
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bbf',
    physical_map_id: 2,
    point_value: 120,
    short_name: 'Room Escapers',
    specific: '',
    location_type: '',
    description:
      'Head to Room Escapers and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc0',
    physical_map_id: 7,
    point_value: 120,
    short_name: 'Spyce',
    specific: '',
    location_type: '',
    description:
      'Head to Spyce and locate a person wearing a Boxaroo shirt for this challenge!',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc1',
    physical_map_id: 8,
    point_value: 25,
    short_name: 'Haymarket',
    specific: 'Intersection of Surface Rd. and Hanover St.',
    location_type: '',
    description:
      "In this crosswalk, there's a metal newspaper from the Boston Herald American. Which U.S. president is mentioned in the headlines?",
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc2',
    physical_map_id: 8,
    point_value: 25,
    short_name: 'Haymarket',
    specific: 'In the alleyway behind Room Escapers and The Point',
    location_type: '',
    description:
      'At the base of this alleyway, there is a stone that was originally used for grinding paint pigments. What year is engraved below the stone?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc3',
    physical_map_id: 9,
    point_value: 25,
    short_name: 'New England Holocaust Memorial',
    specific: 'Between Congress St. and Union St.',
    location_type: '',
    description:
      'The New England Holocaust Memorial was built for a number of reasons, one of which is engraved into the ground before you enter the memorial. What reason is that?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc4',
    physical_map_id: 10,
    point_value: 30,
    short_name: 'Court Street Starbucks',
    specific: '',
    location_type: '',
    description:
      'Those who frequent Starbucks know that a tall is a small, a grande is a medium, and venti is a large. On top of this Starbucks beside Boxaroo is a sculpture that holds a lot more liquids than all of those combined. How many gallons does this sculpture hold?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc5',
    physical_map_id: 11,
    point_value: 40,
    short_name: 'Old City Hall',
    specific: 'Courtyard statues',
    location_type: '',
    description:
      "In front of the city hall is a statue often thought to be associated with a particular political party. Though that wasn't the purchaser's original intent for the statue, he quickly put in something to represent the other party. Take a photo with your entire team touching one or both of these statues.",
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc6',
    physical_map_id: 12,
    point_value: 25,
    short_name: 'Old State House',
    specific: '',
    location_type: '',
    description:
      'This building features two creatures climbing up to the roof.  One is a mythical beast that would be right at home in Bostonshire.  What is the other?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc7',
    physical_map_id: 13,
    point_value: 25,
    short_name: '27 Devonshire',
    specific: '',
    location_type: '',
    description:
      'Three preachers presided over the First Meeting House in Boston.  One came from a family of volleyballs, one made friends with ET, and one was named for a plant used to make textiles.  They all shared a first name.  What was it?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc8',
    physical_map_id: 14,
    point_value: 60,
    short_name: 'State Street T entrance on Congress Street',
    specific: 'Series of mirrored columns',
    location_type: '',
    description:
      'Take a picture of at least 9 members of your team. Yes, we know that there are at most 5 people on your team.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bc9',
    physical_map_id: 16,
    point_value: 30,
    short_name: 'Liberty Square',
    specific: '',
    location_type: '',
    description:
      'This unusually curvy building next to the Square was named for someone who was apparently really good at growing large fruit.  Who was it?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bca',
    physical_map_id: 16,
    point_value: 40,
    short_name: 'Liberty Square',
    specific: '',
    location_type: '',
    description:
      'Oh, no!  The woman climbing the scary mound of faces will drop that baby if the backbending soldier trips her up!  Take a photo of your team ready to catch the baby.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bcb',
    physical_map_id: 17,
    point_value: 35,
    short_name: 'Angell Memorial Square',
    specific: 'Bronze animal bricks',
    location_type: 'Public art',
    description:
      'Take a picture of your team standing on the bronze tiles, acting out their respective animals.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bcc',
    physical_map_id: 15,
    point_value: 35,
    short_name: 'Water \u0026 Congress Streets',
    specific: 'The Liberator plaque',
    location_type: '',
    description:
      'The Liberator was an anti-slavery newspaper based here from 1831 to 1834.  When it moved its base of operations, what was presumably the main vegetable grown in its new home?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bcd',
    physical_map_id: 17,
    point_value: 25,
    short_name: 'Angell Memorial Square',
    specific: 'Statue and plaques',
    location_type: 'Memorial',
    description:
      'George Thorndike Angell is remembered here for his founding of the Massachusetts Society for the Prevention Cruelty to Animals. Atop the pillar is a golden eagle, but what animal is depicted four times at the base?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bce',
    physical_map_id: 17,
    point_value: 35,
    short_name: 'Angell Memorial Square',
    specific: '',
    location_type: '',
    description:
      "The four lions in this sculpture look agitated, but they are warm and gentle creatures to those whom they trust. Demonstrate a soothing connection by taking a photo of your team touching the lions' snouts.",
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bcf',
    physical_map_id: 18,
    point_value: 30,
    short_name: 'Norman B. Leventhal Park',
    specific: '',
    location_type: '',
    description:
      'The friendly trees here are wearing name tags.  Who is Quercus Palustris?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd0',
    physical_map_id: 20,
    point_value: 40,
    short_name: 'Faneuil Hall',
    specific: '',
    location_type: '',
    description:
      "There is a very unusual creature on the weathervane on top of Faneuil Hall.  Prove that you are a real Bostonian by telling us what it is.  Do not Drowne in your sorrows if you don't know.",
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd1',
    physical_map_id: 19,
    point_value: 40,
    short_name: '225 Franklin Street',
    specific: 'Public sculpture near Equinox',
    location_type: 'Public art',
    description:
      "In the same block, there is a company called Columbia Threadneedle. Find the public art on the opposite side of the building and take a photo doing as the company's name suggests.",
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd2',
    physical_map_id: 20,
    point_value: 30,
    short_name: 'Faneuil Hall - Samuel Adams Statue',
    specific: 'Near Congress St.',
    location_type: '',
    description:
      "Let's see a picture of your best statesman pose next to Boston's most famous statesman.",
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd3',
    physical_map_id: 20,
    point_value: 30,
    short_name: 'Faneuil Hall - Larry Bird Statue',
    specific: 'On South Market St.',
    location_type: '',
    description:
      'This avian man has a plaque near his statue commemorating his acceptance into the Basketball Hall of Fame. What number did he wear on his jersey?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd4',
    physical_map_id: 21,
    point_value: 30,
    short_name: '160 State Street',
    specific: '',
    location_type: '',
    description:
      'The Black Rose bar has proudly been serving a particular Irish Stout since a given year. That Irish stout has been around since 1759. What is the difference between these two years?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd5',
    physical_map_id: 22,
    point_value: 25,
    short_name: 'The Upper, Upper Greenway',
    specific: 'In The North End Parks',
    location_type: '',
    description:
      'This area has beams to represent the highway that covered the city here before the Big Dig. Take a photo of your team mimicking one of these beam structures.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd6',
    physical_map_id: 23,
    point_value: 40,
    short_name: 'The Upper Greenway',
    specific: 'Greenway Carousel',
    location_type: '',
    description:
      'There is a diverse array of creatures here that move up and down, but otherwise always follow the same animal.  What animal probably most regrets this arrangement? This animal might not come out smelling the best. ',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd7',
    physical_map_id: 24,
    point_value: 15,
    short_name: 'The Middle Greenway',
    specific: 'GLOW art exhibition',
    location_type: '',
    description:
      'This challenge pertains to the neon signs! Whose restaurant features a chicken?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd8',
    physical_map_id: 24,
    point_value: 15,
    short_name: 'The Middle Greenway',
    specific: 'GLOW art exhibition',
    location_type: '',
    description:
      'This challenge pertains to the neon signs! Where might you go to get a bike?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bd9',
    physical_map_id: 24,
    point_value: 15,
    short_name: 'The Middle Greenway',
    specific: 'GLOW art exhibition',
    location_type: '',
    description:
      'This challenge pertains to the neon signs! This restaurant has a rocket in its logo, though the name of the restaurant evokes a much-loved New England train. What kind of a train?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bda',
    physical_map_id: 24,
    point_value: 15,
    short_name: 'The Middle Greenway',
    specific: 'GLOW art exhibition',
    location_type: '',
    description:
      'This challenge pertains to the neon signs! The sign for the motel in Saugus was one of many along the Saugus strip. The strip also had a sign for a mini-golf course with a particular orange creature looming over it. What was the orange creature?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bdb',
    physical_map_id: 24,
    point_value: 15,
    short_name: 'The Middle Greenway',
    specific: 'GLOW art exhibition',
    location_type: '',
    description:
      'This challenge pertains to the neon signs! This potato chip factory had the slogan "Delivered fresh, the morning after they\'re made". How many delivery trucks did they have at one point?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bdc',
    physical_map_id: 25,
    point_value: 30,
    short_name: 'The Lower Greenway',
    specific: 'Luftwerk public sculpture',
    location_type: 'Public art',
    description:
      'The I-93 tunnels *should* be underground, but they are reimagined here among the trees. What is the name of this piece of art?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bdd',
    physical_map_id: 26,
    point_value: 35,
    short_name: 'Long Wharf Area',
    specific: 'The Liberty Fleet of Tall Ships',
    location_type: '',
    description:
      'The Liberty Fleet of Tall Ships offers many cruises.  Only one of their cruises does not refer to a particular time of day or week.  That cruise offers two things: a drink that is enjoyed by pirates, and what other thing?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1bde',
    physical_map_id: 26,
    point_value: 30,
    short_name: 'Long Wharf Area',
    specific: '',
    location_type: 'Public art',
    description:
      'Take a photo of at least 4 people standing on a giant compass and pointing in the approximate direction of their place of birth.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1bdf',
    physical_map_id: 26,
    point_value: 30,
    short_name: 'Long Wharf Area',
    specific: 'Boston Harbor Cruises "Fun Facts" plaques',
    location_type: '',
    description:
      "Utah's state bird is the seagull.  What plague did the seagulls help defeat to become so honored?",
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1be1',
    physical_map_id: 26,
    point_value: 40,
    short_name: 'Long Wharf Area',
    specific: '',
    location_type: '',
    description: 'Take a photo of someone making the catch of a lifetime.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1be2',
    physical_map_id: 27,
    point_value: 30,
    short_name: 'Central Wharf',
    specific: 'Dolphin statue east of New England Aquarium',
    location_type: '',
    description:
      'Take a photo of your team "swimming with the dolphins".  Include at least 4 dolphins.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1be3',
    physical_map_id: 28,
    point_value: 25,
    short_name: 'Harbor Towers',
    specific: 'David von Schlegell public sculpture',
    location_type: 'Public art',
    description:
      'David von Schlegell started working in metals in 1964, seeking to explore the dynamic between art and engineering. What excess from abstract expressionism did he want to pare from his work?',
    challenge_type: 'text'
  },
  {
    _id: '5b788a7ed10c2a62562e1be4',
    physical_map_id: 29,
    point_value: 40,
    short_name: '50 Rowes Wharf',
    specific: 'Boston Harbor Hotel archway',
    location_type: '',
    description:
      'Take a photo of your shortest team member entirely filling this beautiful archway.  Tricks of perspective may be necessary.',
    challenge_type: 'image'
  },
  {
    _id: '5b788a7ed10c2a62562e1be5',
    physical_map_id: 30,
    point_value: 25,
    short_name: 'US Coast Guard Building',
    specific: 'Captain John Foster Williams plaque',
    location_type: '',
    description:
      'Captain John Foster Williams began his sailing career at the tender age of 15.  What dangerous ship did he command during the Revolutionary War?',
    challenge_type: 'text'
  }
];
