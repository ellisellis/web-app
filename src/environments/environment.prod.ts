export const environment = {
  production: true,
  apiHost: `https://api.devlemur.com`,
  imageHost: `https://bostonshire-3.nyc3.digitaloceanspaces.com`,
  sw: false
};
